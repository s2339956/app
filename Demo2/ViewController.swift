//
//  ViewController.swift
//  Demo2
//
//  Created by 謝易致 on 2016/4/20.
//  Copyright © 2016年 Tin_XIS. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var tableView:UITableView!
    var ObjectArray = [String]()

    var i=0

    var head:XHPathCover!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        self.tableView = UITableView(frame: self.view.frame)
        self.view.addSubview(self.tableView)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        self.tableView.tableFooterView = UIView()
        
        for i in 0..<10 {
            self.ObjectArray.append("\(i)")
        }
        
    //    self.tableView.addLegendHeaderWithRefreshingTarget(self, refreshingAction: "headRefresh")
        self.tableView.addGifFooterWithRefreshingTarget(self, refreshingAction: #selector(ViewController.footRefresh))
        
        head = XHPathCover(frame: CGRectMake(0, 0, 320, 200))
        head.setBackgroundImage(UIImage(named: "BG"))
        head.setAvatarImage(UIImage(named: "cute_girl.jpg"))
        head.isZoomingEffect = true;
        head.setInfo([XHUserNameKey : "StrongX"])
        head.setInfo([XHBirthdayKey : "iOS工程師"])
        head.avatarButton.layer.cornerRadius = 33;
        head.avatarButton.layer.masksToBounds = true
        head.handleRefreshEvent = {
            self.headRefresh()
        }
        tableView.tableHeaderView = head;
        
        
        
        SDImageCache.sharedImageCache().clearDisk()
        SDImageCache.sharedImageCache().clearMemory()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        head.scrollViewDidScroll(scrollView)
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        head.scrollViewDidEndDecelerating(scrollView)
    }
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        head.scrollViewDidEndDragging(scrollView, willDecelerate: decelerate)
    }
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        head.scrollViewWillBeginDragging(scrollView)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjectArray.count
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) 

        for view in cell.contentView.subviews {
            view.removeFromSuperview()
        }
        
        let image = UIImageView(frame: CGRectMake(10, 10, 60, 60))
        image.layer.cornerRadius = 30
        image.layer.masksToBounds = true
        image.sd_setImageWithURL(NSURL(string: "http://ac-hn8w3hlp.clouddn.com/fVju6pA4WGzVGNGsVdVXEzB.png"), placeholderImage: UIImage(named: "cute_girl.jpg"))
        cell.contentView.addSubview(image)
        
        let label = UILabel(frame: CGRectMake(80, 30, 100, 20))
        label.text = "這是第\(indexPath.row)行"
        cell.contentView.addSubview(label)
        
        
        
        return cell
    }
    func headRefresh(){
        ProgressHUD.show("親愛的，別急嘛～～～")
        self.Delay(2, closure: { () -> () in
            self.ObjectArray.removeAll(keepCapacity: false)

            for i in 0..<10 {
                self.ObjectArray.append("\(i)")
            }
       //     self.tableView.header.endRefreshing()
            self.tableView.reloadData()
            self.head.stopRefresh()
            ProgressHUD.showSuccess("人家準備好了！")
        })
    }
    func footRefresh(){
        ProgressHUD.show("還有更多内容")
        self.Delay(2, closure: { () -> () in
            let j = self.i + 10
            for i in 0..<j {
                self.ObjectArray.append("\(i)")
            }
            self.tableView.footer.endRefreshing()
            self.tableView.reloadData()
            ProgressHUD.showSuccess("好了啦～～")
        })
    }
    func Delay(time:Double,closure:()->()){
       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(time * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), closure)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

